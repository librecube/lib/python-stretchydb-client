from setuptools import setup

setup(
    name='stretchydb-client',
    version='2.1.0',
    description='Python client for the StretchyDb database',
    url='https://gitlab.com/artur-scholz/stretchydb-client',
    author='Artur Scholz',
    author_email='artur.scholz@librecube.org',
    license='MIT',
    python_requires='>=3',
    keywords='database client REST api',
    packages=['stretchydb_client'],
    install_requires=[
        'requests',
        ],
)
